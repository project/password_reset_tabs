
CONTENTS OF THIS FILE
---------------------
 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers

INTRODUCTION
------------

 * Password Reset tabs is a simple UI for changing drupal password.It provides
   a simple workflow,i.e a series of tabular steps on same page.
   For a full description of the module, visit the project page:
  - https://www.drupal.org/sandbox/saurabh.tripathi.cs/2403787

REQUIREMENTS
------------
This module requires the following modules:
 * user (Core module)

INSTALLATION
------------
 * Install as you would normally install a contributed drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.

CONFIGURATION
-------------
 * Password reset tabs does not comes with a configuration panel for now.Though
   you can simply access its functionality from a menu which is:
   www.example.com/password_reset.
   Here you will be able to follow the new forgot password workflow for your
   site.
   Instead of using the default Request new password link you can link this
   menu and change the process of password reset.

MAINTAINER
-----------
Current maintainers:
 * Saurabh Tripathi (saurabh.tripathi.cs) - https://www.drupal.org/user/2436956/
